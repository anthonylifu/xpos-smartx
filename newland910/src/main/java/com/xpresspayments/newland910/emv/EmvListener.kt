package com.xpresspayments.newland910.emv

import android.app.Activity
import android.content.DialogInterface
import android.os.Handler
import android.util.Log
import com.newland.mtype.module.common.emv.EmvTransController
import com.newland.mtype.module.common.emv.EmvTransInfo
import com.newland.mtype.module.common.emv.SecondIssuanceRequest
import com.newland.mtype.module.common.emv.level2.EmvCardholderCertType
import com.newland.mtype.module.common.emv.level2.EmvLevel2ControllerExtListener
import com.newland.mtype.util.ISOUtils
import com.xpresspayments.commons.EmvTag
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.extensions.*
import com.xpresspayments.newland910.ui.PinEntryDialog
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.commons.Reader
import com.xpresspayments.commons.runOnMain
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.TransactionRequestData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.isApproved
import com.xpresspos.epmslib.extensions.padLeft
import com.xpresspos.epmslib.tlv.TLV
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import java.math.BigDecimal

internal class EmvListener(private val handler: NewlandCardProcessor) : EmvLevel2ControllerExtListener, com.xpresspayments.commons.AbstractCoroutineScope() {
    val TAG = javaClass.simpleName

    val context = handler.activity
    private var transactionSequence: Int = 0
    private var encryptedPinBlock: ByteArray? = null

    private val disposable = CompositeDisposable()

    private var hostResponse: TransactionResponse? = null


    override fun isAccountTypeSelectInterceptor(): Boolean {
        return false
    }

    override fun incTsc(): Int {
        return ++transactionSequence
    }

    override fun isEcSwitchInterceptor(): Boolean {
        return false
    }


    override fun cardHolderCertConfirm(p0: EmvCardholderCertType?, p1: String?): Boolean {
        return true
    }

    override fun ecSwitch(): Int {
        return 1;
    }


    override fun isCardHolderCertConfirmInterceptor(): Boolean {
        return false
    }

    override fun accTypeSelect(): Int {
        return 0
    }

    override fun isLCDMsgInterceptor(): Boolean {
        return true
    }

    override fun isTransferSequenceGenerateInterceptor(): Boolean {
        return false
    }

    override fun lcdMsg(title: String, msg: String, yesnoShowed: Boolean, waittingTime: Int): Int {
        var alertDialog: DialogInterface?
        Log.d(
            TAG,
            "lcdMsg  - title::: $title |  msg::: $msg | yesnoShowed:::$yesnoShowed | waitingTime::: $waittingTime"
        )
//        lcdMsg  - title::: PIN 错误 |  msg::: YES(Confirm) | yesnoShowed:::false | waitingTime::: 5
        var wait = true
        var result = 1

        val titleRes = R.string.card_transaction

        val message =
//                if (msg.isNotBlank()) msg else
            context.getString(R.string.incorrect_pin)

        runOnMain {
            if (yesnoShowed) {
                alertDialog = context.alert {
                    isCancelable = false
                    titleResource = titleRes
                    this.message = message
                    okButton {
                        wait = false
                        result = 1
                    }

                    cancelButton {
                        wait = false
                        result = 0
                    }

                }.show()
            } else {
                alertDialog = context.alert {
                    isCancelable = false
                    titleResource = titleRes
                    this.message = message
                }.show()
            }

            Handler().postDelayed({
                if (wait) {
                    alertDialog?.dismiss()
                    result = 0
                    wait = false
                }

            }, waittingTime.toLong() * 1000)


        }

        while (wait); //Wait for result


        return result
    }


    override fun onRequestOnline(controller: EmvTransController, transInfo: EmvTransInfo) {
        Log.d(TAG, "onRequestOnline")
        var alertDialog: DialogInterface? = null


        runOnMain {
            alertDialog = this@EmvListener.context.alert {
                isCancelable = false
                messageResource = R.string.pin_ok
            }.show()
        }

        val track2Data = ISOUtils.hexString(transInfo.track_2_eqv_data)
        val DE55 = context.emvModule.fetchEmvData(com.xpresspayments.commons.EmvTag.nibssRequestTags)
        val panSequenceNumber = transInfo.cardSequenceNumber.padLeft(3, '0')
            val posEntryMode = if (handler.actualReaderUsed == Reader.CTLS) "071" else "051" //TLV.fromRawData(context.emvModule.fetchEmvData(intArrayOf(EmvTag.POS_ENTRY_MODE)).hexByteArray, 0)

        val cardData = CardData(track2Data, DE55, panSequenceNumber, posEntryMode/*"051" posEntryModeTag.value*/).apply {
            this.pinBlock = encryptedPinBlock?.hexString
        }
        Log.d(TAG, cardData.toString())

        launch {
            delay(2000)
            alertDialog?.dismiss()
//            handler.setState(ProcessState(ProcessState.States.PROCESSING, "Processing Online"))
            doOnlineProcessing(controller, handler.activity, handler.requestData, cardData)
            encryptedPinBlock = null
        }

    }

    private fun doOnlineProcessing(
        controller: EmvTransController,
        activity: Activity,
        requestData: TransactionRequestData,
        cardData: CardData
    ) {
        val result = kotlin.runCatching {
            handler.processor.processTransaction(
                activity,
                requestData,
                cardData
            )

//            processor.rollback(activity).blockingGet()
        }

        if (result.isFailure) {
            result.exceptionOrNull()?.let {
                Log.e(TAG, "Host request error")
                it.printStackTrace()
                controller.secondIssuance(SecondIssuanceRequest().apply {
                    setAuthorisationResponseCode("20")  //set invalid response code
                })
            }
        }

        if (result.isSuccess) {
            hostResponse = result.getOrNull()
            println(hostResponse)
            hostResponse?.let {
                val secondIssuanceRequest = SecondIssuanceRequest().apply {
                    this.setAuthorisationResponseCode(it.responseCode)
                    if (it.authCode.isNotBlank()) {
                        this.setAuthorisationCode(it.authCode)
                    }

                    if (!it.responseDE55.isNullOrBlank()) {
                        this.field55 = it.responseDE55!!.hexByteArray
                    }

                }
                controller.secondIssuance(secondIssuanceRequest)
            }
        }
    }

    override fun onRequestPinEntry(controller: EmvTransController, transInfo: EmvTransInfo?) {
        Log.d(TAG, "onRequestPinEntry")
        val cardPan = transInfo?.cardNo ?: kotlin.run {
            Log.d(TAG, "Could not retrieve card no")
            controller.cancelPinInput()
            return
        }

        val isOffline = transInfo.isOfflinePin
        val modulus = transInfo.modulus
        val exponent = transInfo.exponent

        runOnMain {
            val pinDialog = PinEntryDialog.newInstance(cardPan, modulus, exponent, isOffline,
                    (handler.requestData.amount + handler.requestData.otherAmount)).apply {
                pinResult.subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { pin, throwable ->
                        throwable?.let {
                            it.printStackTrace()
                            controller.sendPinInputResult(null)
                        }

                        pin?.let {
                            controller.sendPinInputResult(it)
                            if (!isOffline) {
                                encryptedPinBlock = it
                            }
                        }
                    }
            }

            pinDialog.show(context.supportFragmentManager, "Pin Dialog")
        }

    }

    override fun onRequestAmountEntry(controller: EmvTransController, transInfo: EmvTransInfo?) {
        Log.d(TAG, "onRequestAmountEntry")

        controller.sendAmtInputResult(BigDecimal(handler.requestData.amount))
    }

    override fun onEmvFinished(status: Boolean, transInfo: EmvTransInfo?) {

        Log.d(
            "onEmvFinished",
            "Status: $status ::: Execute Result: ${transInfo?.executeRslt} ::: Error Code: ${transInfo?.errorcode}"
        )
        encryptedPinBlock = null

        if (hostResponse == null) {
            handler.setError(RuntimeException("Transaction Failed"))
            destroy()
            return
        }

        if (hostResponse!!.isApproved && !status) {
            handler.setState(ProcessState(ProcessState.States.PROCESSING, "Processing Online"))
            hostResponse = handler.processor.rollback(handler.activity)
        }

        hostResponse?.let {
            it.AID = transInfo?.aid?.hexString ?: ""
            it.TVR = transInfo?.terminalVerificationResults?.hexString ?: ""
            it.TSI = transInfo?.transaction_status_information?.hexString ?: ""

            val cardHolderVal = context.emvModule.getEmvData(com.xpresspayments.commons.EmvTag.CARDHOLDER_NAME_5F20)?: byteArrayOf()
            it.cardHolder = String(transInfo?.cardHolderName ?:  cardHolderVal)

            it.cardLabel = transInfo?.application_label ?: ""
            it.appCryptogram = transInfo?.appCryptogram?.hexString ?: ""
        }

        val resultMsgId = transInfo?.resultMessageId
        //The specific reason for the error code
        val addInfoId = transInfo?.errorMessageId


        hostResponse?.let(handler::setResponse)
        destroy()
    }

    override fun onError(p0: EmvTransController?, e: Exception?) {
        encryptedPinBlock = null
        Log.d(TAG, "onError")
        handler.setError(e ?: RuntimeException("Transaction Failed"))
        destroy()
    }

    override fun onRequestTransferConfirm(controller: EmvTransController, transInfo: EmvTransInfo?) {
        Log.d(TAG, "onRequestTransactionConfirm")
        controller.transferConfirm(true)
    }

    override fun onFallback(transInfo: EmvTransInfo?) {
        Log.d(TAG, "onFallback")
        val reader = handler.actualReaderUsed?.let {
            when (it) {
                com.xpresspayments.commons.Reader.CT -> com.xpresspayments.commons.Reader.MSR
                else -> com.xpresspayments.commons.Reader.CT
            }
        } ?: com.xpresspayments.commons.Reader.CT

        handler.startTransaction(handler.requestData, reader)
    }

    override fun onRequestSelectApplication(controller: EmvTransController, transInfo: EmvTransInfo?) {
        Log.d(TAG, "onRequestSelectApplication")

        transInfo?.let {
            val aidNameList: List<String> = transInfo.aidSelectMap.values.map { it.name }

            runOnMain {
                context.alert {
                    isCancelable = false
                    titleResource = R.string.select_emv_app
                    items(aidNameList) { _, index ->
                        controller.selectApplication(transInfo.aidSelectMap.keys.elementAt(index))
                    }

                    cancelButton {
                        controller.cancelEmv()
                    }
                }.show()
            }
        } ?: kotlin.run {
            controller.cancelEmv()
        }
    }

    override fun isLanguageselectInterceptor(): Boolean {
        return false
    }

    override fun languageSelect(p0: ByteArray?, p1: Int): ByteArray {
        return byteArrayOf()
    }

    override fun destroy() {
        disposable.clear()
        super.destroy()
    }

}