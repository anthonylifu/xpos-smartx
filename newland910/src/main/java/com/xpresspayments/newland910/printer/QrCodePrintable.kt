package com.xpresspayments.newland910.printer

class QrCodePrintable(val content: String, val align: Printable.Align = Printable.Align.LEFT, val alternateSize: Int =  0): Printable {

    override fun prePrint() = StringBuilder().apply {
        if (alternateSize > 0) {
            append("!qrcode $alternateSize 2\n")
        }

        append("*qrcode $align $content\n")
    }.toString()
}