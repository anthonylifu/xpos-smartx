package com.xpresspayments.newland910.printer

import android.graphics.Bitmap

data class BitmapPrintable (val key: String,
                            val bitmap: Bitmap,
                            val width: Int,
                            val height: Int,
                            val align: Printable.Align = Printable.Align.LEFT): Printable{

    val data: Pair<String, Bitmap>
        get() = Pair(key, bitmap)

    override fun prePrint() = "*image $align $width*$height path:$key\n"

}