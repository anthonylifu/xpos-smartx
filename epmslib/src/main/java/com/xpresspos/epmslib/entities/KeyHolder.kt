package com.xpresspos.epmslib.entities

import android.os.Parcelable
import android.util.Log
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.xpresspos.epmslib.extensions.xor
import com.xpresspos.epmslib.utils.TripleDES
import com.xpresspos.epmslib.utils.Utility
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue


@Parcelize
@Entity
data class KeyHolder
/**
 *
 * @param masterKey
 * @param sessionKey
 * @param pinKey
 * @param track2Key
 * @param bdk
 * @param isTest
 */
(var masterKey: String = "", var sessionKey: String = "", var pinKey: String = "", var track2Key: String = "",
 var bdk: String = ""): Parcelable {

    @PrimaryKey
    var id = 1
}


val KeyHolder.clearSessionKey: String
    get() = TripleDES.decrypt(this.sessionKey, masterKey)

val KeyHolder.clearPinKey: String
    get() = TripleDES.decrypt(this.pinKey, masterKey)
