package com.xpresspayments.commons

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.VibrationEffect.DEFAULT_AMPLITUDE
import android.os.Vibrator
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.snackbar.Snackbar


val Context.vibrator
    get() = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator


fun <T: Preference> PreferenceFragmentCompat.findPreference(keyId: Int) = findPreference<T>(getString(keyId))
fun PreferenceFragmentCompat.getEditTextPreferenceValue(keyId: Int, defaultValue: String = "") =
        findPreference<EditTextPreference>(keyId)?.text ?: defaultValue

fun PreferenceFragmentCompat.getEditTextPreferenceValue(keyId: Int, defaultValueId: Int) = getEditTextPreferenceValue(keyId, getString(defaultValueId))

fun Fragment.showSnack(message: String, duration: Int = Snackbar.LENGTH_SHORT) =
        Snackbar.make(view!!, message, duration).show()

val Fragment.navController
    get() =  findNavController()

fun Context.vibrate(durationInMillis: Long = 500, amplitude: Int = DEFAULT_AMPLITUDE) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        vibrator.vibrate(VibrationEffect.createOneShot(durationInMillis, amplitude))
    } else {
        vibrator.vibrate(durationInMillis)
    }
}

fun logTrace(text: String) =  Log.d("Smartx", text)  //Bugfender.d("Smartx", text)


