package com.xpresspayments.commons.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.xpresspayments.commons.R
import com.xpresspayments.commons.vibrate
import kotlinx.android.synthetic.main.number_pad_layout.view.*


class Keypad : LinearLayout {

   private var output: TextView? = null
    private var outputResId: Int = -1

    private var isNumberPad = false


    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.number_pad_layout, this)

        attrs?.let {
            context.theme.obtainStyledAttributes(it, R.styleable.Keypad, 0, 0).apply {
                isNumberPad = this.getBoolean(R.styleable.Keypad_isNumberPad, false)
                outputResId = this.getResourceId(R.styleable.Keypad_receiver, -1)
            }
        }

        updateUI()

        btn_0.setOnClickListener{
            onPress("0")
        }
        btn_1.setOnClickListener{
            onPress("1")
        }
        btn_2.setOnClickListener{
            onPress("2")
        }
        btn_3.setOnClickListener{
            onPress("3")
        }
        btn_4.setOnClickListener{
            onPress("4")
        }
        btn_5.setOnClickListener{
            onPress("5")
        }
        btn_6.setOnClickListener{
            onPress("6")
        }
        btn_7.setOnClickListener{
            onPress("7")
        }
        btn_8.setOnClickListener{
            onPress("8")
        }
        btn_9.setOnClickListener{
            onPress("9")
        }

        btn_00.setOnClickListener {
            onPress("00")
        }
        btn_Del.setOnClickListener{
            val content = output?.text
            if (!content.isNullOrEmpty()) {
                vibrate()
                output?.text = content.subSequence(0, content.length-1)
            }
        }
    }


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        output = (parent as? View)?.findViewById(outputResId)
    }

    private fun updateUI() {
        if (!isNumberPad) {
            btn_00.visibility = View.GONE
            btn_0.layoutParams = LayoutParams(0,LayoutParams.MATCH_PARENT, 2.0F)
        } else {
            btn_00.visibility = View.VISIBLE
            btn_0.layoutParams = btn_00.layoutParams
        }
        invalidate()
    }

    private fun onPress(input: String) {
        vibrate()
        output?.append(input)
    }

    private fun vibrate() {
        context?.vibrate()
    }

    fun  setIsNumberPad(isNumberPad: Boolean) {
        this.isNumberPad = isNumberPad
        updateUI()
    }

    fun isNumberPad() = isNumberPad

    fun setOutputReceiver(textView: TextView) {
        this.output = textView
    }


}
