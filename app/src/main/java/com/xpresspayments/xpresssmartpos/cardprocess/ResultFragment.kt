package com.xpresspayments.xpresssmartpos.cardprocess


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xpresspayments.xpresssmartpos.R
import kotlinx.android.synthetic.main.fragment_result.*


const val STATUS = "status"
const val MESSAGE = "message"

class ErrorFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let { args ->

            val status = args.getBoolean(STATUS, false)
            val message = args.getString(MESSAGE, "")

            if (status) {
                statusImage.setImageDrawable(resources.getDrawable(R.drawable.ic_approved_circle_black_24dp, null))
                titleText.setText(R.string.approved)
            } else {
                statusImage.setImageDrawable(resources.getDrawable(R.drawable.ic_error_black_24dp, null))
                titleText.setText(R.string.declined)
            }

            messageText.text = message
        }

    }
}

