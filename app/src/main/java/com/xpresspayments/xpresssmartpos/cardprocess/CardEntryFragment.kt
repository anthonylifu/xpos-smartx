package com.xpresspayments.xpresssmartpos.cardprocess

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xpresspayments.xpresssmartpos.R
import kotlinx.android.synthetic.main.fragment_card_entry.*


const val INSERT_MESSAGE = "insert_message"

class CardEntryFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_card_entry, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        messageText.text = arguments?.getString(INSERT_MESSAGE) ?: getString(R.string.insert_or_tap_card)

        cancelBtn.setOnClickListener {
            (activity as? TransactionAborter)?.abortTransaction()
        }
    }

}
