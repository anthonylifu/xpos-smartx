package com.xpresspayments.xpresssmartpos.ui

import android.app.Dialog
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.xpresspayments.xpresssmartpos.R
import com.xpresspos.epmslib.utils.IsoAccountType

// TODO: Customize parameter argument names
const val ARG_ITEM_COUNT = "item_count"

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    IsoAccountTypeListDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 */
class IsoAccountTypeListDialogFragment(private val onSelectAccount: (IsoAccountType) -> Unit) : BottomSheetDialogFragment() {

    private val accountTypes = arrayListOf(IsoAccountType.DEFAULT_UNSPECIFIED, IsoAccountType.SAVINGS, IsoAccountType.CURRENT, IsoAccountType.CREDIT)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_accounttype_list_dialog_list_dialog,
            container,
            false
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
       with(view.findViewById<RecyclerView>(R.id.list)) {
           layoutManager = LinearLayoutManager(context)
           adapter = IsoAccountTypeAdapter()
       }

    }

    private inner class ViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        inflater.inflate(
            R.layout.fragment_accounttype_list_dialog_list_dialog_item,
            parent,
            false
        )
    ) {

        internal val text: TextView = itemView.findViewById(R.id.text)
    }



    private inner class IsoAccountTypeAdapter  : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.text.text = accountTypes[position].toString()
            holder.text.setOnClickListener {
                onSelectAccount(accountTypes[position])
                dismiss()
            }
        }

        override fun getItemCount(): Int {
            return accountTypes.size
        }
    }
}