package com.xpresspayments.xpresssmartpos.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.xpresspayments.xpresssmartpos.R
import kotlinx.android.synthetic.main.fragment_password_dialog.*


class PasswordDialogFragment(val title: String) : DialogFragment() {


    private val content = MutableLiveData<String>()

    val password: LiveData<String>
        get() = content

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_password_dialog, container, false)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_PopupOverlay)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subtitleText.text = title
        nextBtn.setOnClickListener {
            if (passwordText.length() > 0) {
                content.postValue(passwordText.text.toString())
            } else {

            }
        }
    }
}