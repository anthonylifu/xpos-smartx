package com.xpresspayments.xpresssmartpos

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Process
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.verifonex990.verifone.extensions.logTrace
import com.xpresspayments.xpresssmartpos.extensions.showInfo
import com.xpresspayments.xpresssmartpos.extensions.validateAdminAccess
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(),
        PreferenceFragmentCompat.OnPreferenceStartFragmentCallback,
        ActionBarTitleUpdater {

    private val navController by lazy {
        findNavController(R.id.navHost)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupActionBarWithNavController(navController, AppBarConfiguration(setOf(R.id.mainFragment,
                R.id.transactionsFragment, R.id.reportFragment)))
        bottomNavView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->

            bottomNavView.visibility =  when (destination.id) {
                 R.id.transactionsFragment, R.id.reportFragment -> View.VISIBLE
                else -> View.GONE
            }

            if ( destination.id == R.id.mainFragment) {
                supportActionBar?.hide()
            } else {
                supportActionBar?.show()
            }
            logTrace("Process Id: ${Process.myPid()}")
        }

        checkLocationPermission()
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                android.R.id.home -> navController.navigateUp()
                R.id.settingsFragment -> {
                    validateAdminAccess{
                        navController.navigate(item.itemId)
                    }

                    return true
                }
            }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigateUp() = navController.navigateUp()

    override fun updateTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
        when (pref.key) {
            getString(R.string.xm_config_key) -> {
                val arg = pref.extras
                navController.navigate(R.id.XMSConfigFragment, arg)
                return true
            }

            getString(R.string.host_config_key) -> {
                val arg = pref.extras
                navController.navigate(R.id.hostConfigFragment, arg)
                return true
            }

            getString(R.string.password_config_key) -> {
                val arg = pref.extras
                navController.navigate(R.id.passwordConfigFragment, arg)
                return true
            }
        }

        return false
    }


    private fun checkLocationPermission() {
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            -> {
            }
           ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) -> {
                showInfo("Please permit access to location to enable us tailor our services to you.\n\n Thank you.", "Permission Request") {
                    ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 100)
                }
            }
            else -> {
                val launcher =  registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    logTrace("Location permission granted = $it")
                }
                launcher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }
    }



}


fun Activity.showProgress(title: String?, message: String) = (this.applicationContext as App).showProgress(this, title, message)
fun Activity.dismissProgress() = (this.applicationContext as App).dismissProgress()

fun Activity.onErrorMessage(title: Int, throwable: Throwable) {
    dismissProgress()
    com.xpresspayments.commons.runOnMain {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(if (throwable is StringIndexOutOfBoundsException) getString(R.string.invalid_response) else throwable.localizedMessage)
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}

fun Activity.onSuccessMessage(message: Int, title: Int = R.string.successful) {
    dismissProgress()
    com.xpresspayments.commons.runOnMain {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }
}

