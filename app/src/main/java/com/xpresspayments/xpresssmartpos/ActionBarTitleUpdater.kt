package com.xpresspayments.xpresssmartpos

interface ActionBarTitleUpdater {
    fun updateTitle(title: String)
}