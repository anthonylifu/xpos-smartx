package com.xpresspayments.xpresssmartpos.services


import com.google.gson.annotations.SerializedName

data class HostKeys(
    @SerializedName("CombinedKey")
    val combinedKey: String,
    @SerializedName("Kcv")
    val kcv: String
)