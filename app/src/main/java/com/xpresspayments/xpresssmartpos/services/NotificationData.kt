package com.xpresspayments.xpresssmartpos.services


import com.google.gson.annotations.SerializedName

data class NotificationData(
    @SerializedName("accounttype")
    val accounttype: Int,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("aurhorisationresponse")
    val aurhorisationresponse: String,
    @SerializedName("authorizationCode")
    val authorizationCode: String,
    @SerializedName("customerName")
    val customerName: String,
    @SerializedName("customerOtherInfo")
    val customerOtherInfo: String,
    @SerializedName("datelocaltransaction")
    val datelocaltransaction: String,
    @SerializedName("gpsinfo")
    val gpsinfo: String,
    @SerializedName("iccdata")
    val iccdata: String,
    @SerializedName("merchantId")
    val merchantId: String,
    @SerializedName("mti")
    val mti: String,
    @SerializedName("narration")
    val narration: String,
    @SerializedName("pan")
    val pan: String,
    @SerializedName("posconditioncode")
    val posconditioncode: String,
    @SerializedName("posentrymode")
    val posentrymode: String,
    @SerializedName("processingcode")
    val processingcode: String,
    @SerializedName("refcode")
    val refcode: String,
    @SerializedName("responcecode")
    val responcecode: String,
    @SerializedName("responsedescription")
    val responsedescription: String,
    @SerializedName("revenueCode")
    val revenueCode: String,
    @SerializedName("rrn")
    val rrn: String,
    @SerializedName("seqno")
    val seqno: String,
    @SerializedName("stan")
    val stan: String,
    @SerializedName("t_status")
    val tStatus: String,
    @SerializedName("terminalid")
    val terminalid: String,
    @SerializedName("timelocaltransaction")
    val timelocaltransaction: String,
    @SerializedName("track1")
    val track1: String,
    @SerializedName("track2")
    val track2: String,
    @SerializedName("tranTypeCode")
    val tranTypeCode: String,
    @SerializedName("trancode")
    val trancode: String,
    @SerializedName("transactiondate")
    val transactiondate: String,
    @SerializedName("transactiontime")
    val transactiontime: String,
    @SerializedName("trantype")
    val trantype: String,
    @SerializedName("paymentMethod")
    val paymentMethod: String = "2",
    @SerializedName("batchno")
    val batchno: String = "1",
    @SerializedName("cellinfo")
    val cellinfo: String = "",
    @SerializedName("channelCode")
    val channelCode: String = "2",
    @SerializedName("currencycode")
    val currencycode: String = "566",
    @SerializedName("searchtext")
    val searchtext: String = "",
    @SerializedName("user")
    val user: String = "tms",
    @SerializedName("password")
    val password: String = "+4KJVvZb9s7h+RO1DVzSWg==",
    @SerializedName("otherterminalid")
    val otherterminalid: String = ""
)