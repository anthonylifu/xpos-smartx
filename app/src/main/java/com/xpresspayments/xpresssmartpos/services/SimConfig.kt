package com.xpresspayments.xpresssmartpos.services


import com.google.gson.annotations.SerializedName

data class SimConfig(
    @SerializedName("APN")
    val aPN: String,
    @SerializedName("Network")
    val network: String,
    @SerializedName("SerialNo")
    val serialNo: String
)