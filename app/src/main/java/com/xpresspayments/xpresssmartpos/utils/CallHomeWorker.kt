package com.xpresspayments.xpresssmartpos.utils

import android.content.Context
import android.widget.Toast
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.xpresssmartpos.R
import com.xpresspayments.xpresssmartpos.db
import com.xpresspayments.xpresssmartpos.extensions.connectionData
import com.xpresspayments.xpresssmartpos.extensions.prefs
import com.xpresspayments.xpresssmartpos.terminalSerial
import com.xpresspos.epmslib.entities.clearSessionKey
import com.xpresspos.epmslib.processors.TerminalConfigurator
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CallHomeWorker(val context: Context) :
//        Worker(context, workerParameters)
    TimerTask() {
    private val configurator = TerminalConfigurator(context.connectionData)
    private val terminalId = context.prefs.getString(context.getString(R.string.terminal_id_key), "")!!


    companion object {
        const val WORKER_NAME = "callHome"
    }

    override fun run() {
        GlobalScope.launch {
            try {
                runOnMain {
                    Toast.makeText(context, R.string.calling_home, Toast.LENGTH_LONG).show()
                }

                val keyHolder = context.db.keyHolderDao().get()
                configurator.nibssCallHome(context, terminalId,
                        keyHolder!!.clearSessionKey, context.terminalSerial)

                runOnMain {
                    Toast.makeText(context, R.string.call_home_successful, Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                runOnMain {
                    Toast.makeText(context, R.string.call_home_failed, Toast.LENGTH_SHORT).show()
                }
            }
        }

        NotificationHandler.rePushNotification(context)
    }

}