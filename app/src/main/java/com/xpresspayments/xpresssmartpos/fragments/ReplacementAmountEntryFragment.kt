package com.xpresspayments.xpresssmartpos.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.xpresspayments.xpresssmartpos.R
import com.xpresspayments.xpresssmartpos.extensions.formatSingleExponentCurrency
import com.xpresspayments.xpresssmartpos.extensions.sanitizeAmountString
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartpos.extensions.accountSelectionEnabled
import com.xpresspayments.xpresssmartpos.ui.IsoAccountTypeListDialogFragment
import com.xpresspos.epmslib.entities.OriginalDataElements
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.utils.IsoAccountType
import kotlinx.android.synthetic.main.fragment_amount_entry.*

class ReplacementAmountEntryFragment: AmountEntryFragment() {
    private var originalDataElements: OriginalDataElements? = null


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        originalDataElements = arguments?.getParcelable(getString(R.string.original_data_elements_key)) ?: kotlin.run {
            navController.popBackStack()
            return
        }

        subtitleText.setText(R.string.replacement_amount)

    }

    private fun next(transactionType: TransactionType, accountType: IsoAccountType, originalDataElements: OriginalDataElements, newAmount: Long) {
        navController.navigate(R.id.action_replacementAmountEntryFragment_to_transactionProcessFragment, bundleOf(
                Pair(getString(R.string.transaction_type),transactionType),
                Pair(getString(R.string.transaction_amount),newAmount),
                Pair(getString(R.string.original_data_elements_key), originalDataElements),
                Pair(getString(R.string.account_type), accountType)
        ))
    }

    override fun onClick(v: View?) {
        val amount:Long = amountTextView.text.toString().sanitizeAmountString().replace(".", "").toLongOrNull() ?: 0L
        if(amount <= 0) {
            showSnack(getString(R.string.invalid_amount))
            return
        }

        if (amount > originalDataElements!!.originalAmount) {
            showSnack(getString(R.string.original_amount_entry_error, amount.formatSingleExponentCurrency(),
                    originalDataElements!!.originalAmount.formatSingleExponentCurrency()))
            return
        }

        if (requireContext().accountSelectionEnabled) {
            IsoAccountTypeListDialogFragment{
                next(transactionType, it, originalDataElements!!, amount)
            }.show(requireActivity().supportFragmentManager, "account_select")
        } else {
            next(transactionType, IsoAccountType.DEFAULT_UNSPECIFIED,  originalDataElements!!, amount)
        }
    }
}