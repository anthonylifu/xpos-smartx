package com.xpresspayments.xpresssmartpos.fragments.preferences


import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.core.content.edit
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.xpresspayment.morefun10Q.logTrace
import com.xpresspayments.commons.findPreference
import com.xpresspayments.commons.getEditTextPreferenceValue
import com.xpresspayments.commons.runOnBackground
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartpos.*
import com.xpresspayments.xpresssmartpos.extensions.*
import com.xpresspayments.xpresssmartpos.services.TmsConfig
import com.xpresspayments.xpresssmartpos.services.tmsService
import com.xpresspayments.xpresssmartpos.ui.PasswordUpdateDialog
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.RuntimeException


class PasswordConfigFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.password_config_settings, rootKey)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        super.onCreateOptionsMenu(menu, inflater)

        findPreference<Preference>(R.string.update_admin_password_key)?.setOnPreferenceClickListener {
            updateAdminPassword(it.title?.toString() ?: "")
            true
        }

        findPreference<Preference>(R.string.update_supervisor_password_key)?.setOnPreferenceClickListener {
            updateSupervisorPassword(it.title?.toString() ?: "")
            true
        }

        findPreference<Preference>(R.string.update_operator_password_key)?.setOnPreferenceClickListener {
            updateOperatorPassword(it.title?.toString() ?: "")
            true
        }
    }

    private fun updateAdminPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().adminPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_admin_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }

    private fun updateSupervisorPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().supervisorPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_supervisor_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }

    private fun updateOperatorPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().operatorPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_operator_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }
}
