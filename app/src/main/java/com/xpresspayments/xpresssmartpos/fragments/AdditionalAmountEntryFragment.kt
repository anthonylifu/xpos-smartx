package com.xpresspayments.xpresssmartpos.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartpos.R
import com.xpresspayments.xpresssmartpos.extensions.accountSelectionEnabled
import com.xpresspayments.xpresssmartpos.extensions.formatSingleExponentCurrency
import com.xpresspayments.xpresssmartpos.extensions.sanitizeAmountString
import com.xpresspayments.xpresssmartpos.ui.IsoAccountTypeListDialogFragment
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.utils.IsoAccountType
import kotlinx.android.synthetic.main.button_layout.*
import kotlinx.android.synthetic.main.fragment_amount_entry.*

class AdditionalAmountEntryFragment : AmountEntryFragment() {
    private var amount = 0L

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        amount = arguments?.getLong(getString(R.string.transaction_amount))
                ?: kotlin.run {
                    navController.popBackStack()
                    return
                }

        subtitleText.setText(R.string.cash_back_amount)
    }


    override fun onClick(v: View?) {
        val otherAmount: Long = amountTextView.text.toString().sanitizeAmountString().replace(".", "").toLongOrNull()
                ?: 0L
        if (otherAmount <= 0) {
            showSnack(getString(R.string.invalid_amount))
            return
        }

        if (requireContext().accountSelectionEnabled) {
            IsoAccountTypeListDialogFragment{
                next(transactionType, it, amount, otherAmount)
            }.show(requireActivity().supportFragmentManager, "account_select")
        } else {
            next(transactionType, IsoAccountType.DEFAULT_UNSPECIFIED, amount, otherAmount)
        }
    }

    fun next(transactionType: TransactionType, accountType: IsoAccountType, amount: Long, otherAmount: Long) {
        navController.navigate(R.id.action_additionalAmountEntryFragment_to_transactionProcessFragment, bundleOf(
                Pair(getString(R.string.transaction_type), transactionType),
                Pair(getString(R.string.transaction_amount), amount),
                Pair(getString(R.string.cash_back_amount), otherAmount),
                Pair(getString(R.string.account_type), accountType)
        ))
    }
}