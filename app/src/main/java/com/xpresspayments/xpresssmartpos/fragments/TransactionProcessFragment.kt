package com.xpresspayments.xpresssmartpos.fragments


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.xpresspayments.xpresssmartpos.ActionBarTitleUpdater

import com.xpresspayments.xpresssmartpos.R
import com.xpresspayments.xpresssmartpos.db
import com.xpresspayments.xpresssmartpos.extensions.hostConfig
import com.xpresspayments.commons.navController
import com.xpresspayments.xpresssmartpos.App
import com.xpresspayments.xpresssmartpos.cardprocess.CardTransactionActivity
import com.xpresspayments.xpresssmartpos.ui.TransactionConfirmationData
import com.xpresspayments.xpresssmartpos.ui.TransactionConfirmationViewHolder
import com.xpresspayments.xpresssmartpos.utils.NotificationHandler
import com.xpresspos.epmslib.entities.*
import com.xpresspos.epmslib.utils.IsoAccountType
import kotlinx.android.synthetic.main.fragment_transaction_process.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.util.*


class TransactionProcessFragment : Fragment() {
    lateinit var transactionType: TransactionType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        transactionType = arguments?.get(getString(R.string.transaction_type)) as? TransactionType
                ?: kotlin.run {
                    navController.popBackStack()
                    return
                }

        (activity as ActionBarTitleUpdater).updateTitle(transactionType.toString())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transaction_process, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val amount =  arguments?.getLong(getString(R.string.transaction_amount)) ?: 0L
        if (amount == 0L && transactionType != TransactionType.BALANCE) {
            navController.popBackStack()
            return
        }

        val cashBackAmount =  arguments?.getLong(getString(R.string.cash_back_amount)) ?: 0L
        val originalDataElements = arguments?.getParcelable<OriginalDataElements>(getString(R.string.original_data_elements_key))
        val accountType =  arguments?.getParcelable<IsoAccountType>(getString(R.string.account_type)) ?: IsoAccountType.DEFAULT_UNSPECIFIED

        val view = View.inflate(activity, R.layout.transaction_confirmation_layout, null)
        TransactionConfirmationViewHolder(view).apply {
            val data = TransactionConfirmationData(
                    transactionType = transactionType,
                    RRN = "",
                    amount = amount,
                    otherAmount = cashBackAmount,
                    date = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(Date()),
                    accountType = accountType
            )
            bind(data)
        }

        if ((amount + cashBackAmount) > 0) {
            val alertDialog = MaterialAlertDialogBuilder(requireActivity())
                    .setView(view)
                    .setPositiveButton(R.string.proceed) {_, _ ->
                        startTransaction(transactionType, amount, cashBackAmount, accountType, originalDataElements)
                    }.setNegativeButton(android.R.string.cancel) {_,_ ->
                        navController.popBackStack()
                    }
                    .setCancelable(false)
                    .create()

            alertDialog.show()
        } else {
            startTransaction(transactionType, amount, cashBackAmount, accountType, originalDataElements)
        }

    }


    private fun startTransaction(transactionType: TransactionType, amount: Long, cashBackAmount: Long, accountType: IsoAccountType, originalDataElements: OriginalDataElements?) {
        progressBar.visibility = View.INVISIBLE
        startActivityForResult(Intent(activity, CardTransactionActivity::class.java).apply {

            putExtra(
                    CardTransactionActivity.BUNDLE, bundleOf(
                    Pair(
                            CardTransactionActivity.REQUEST_DATA,
                            TransactionRequestData(transactionType = transactionType,
                                    amount = amount,
                                    otherAmount = cashBackAmount,
                                    accountType = accountType,
                                    originalDataElements = originalDataElements)
                    ),
                    Pair(CardTransactionActivity.HOST_CONFIG, requireContext().hostConfig),
                    Pair(CardTransactionActivity.READERS, com.xpresspayments.commons.Reader.CT_CTLS_MSR)
            )
            )
        }, 100)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        progressBar.visibility = View.VISIBLE

        if (requestCode == 100) {
            if (resultCode != Activity.RESULT_OK) {
                val error = data?.getSerializableExtra(CardTransactionActivity.ERROR) as? Throwable ?: RuntimeException("Unknown Error")
                error.printStackTrace()
            } else {
                val response =  data?.getParcelableExtra<TransactionResponse>(CardTransactionActivity.TRANSACTION_RESPONSE) ?: kotlin.run {
                    throw IllegalStateException("No response received")
                }

                NotificationHandler.sendNotification(requireContext().applicationContext, response)

                navController.navigate(R.id.action_transactionProcessFragment_to_receiptViewFragment, bundleOf(
                        Pair(getString(R.string.response_key), response)
                ))

                return
            }
        }

        navController.popBackStack(R.id.debitCardTransactionFragment,false)
    }

}
//val originalDataElements = OriginalDataElements(
//        originalTransactionType = TransactionType.PRE_AUTHORIZATION,
//        originalAmount = 2500L,
//        originalForwardingInstCode = "424367",
//        originalAcquiringInstCode = "476173",
//        originalAuthorizationCode = "097617",
//        originalSTAN = "101600",
//        originalTransmissionTime = "0125101600"
//)