package com.xpresspayments.xpresssmartpos.repository

import androidx.room.*
import com.xpresspos.epmslib.entities.ConfigData
import io.reactivex.Single
import kotlinx.coroutines.Deferred


@Dao
interface ConfigDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(data: ConfigData)

    @Query("SELECT * FROM ConfigData LIMIT 1")
    fun get(): ConfigData?

    @Delete
    fun delete(configData: ConfigData)
}