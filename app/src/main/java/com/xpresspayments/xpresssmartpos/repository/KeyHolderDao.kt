package com.xpresspayments.xpresssmartpos.repository

import androidx.room.*
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.KeyHolder
import io.reactivex.Single
import kotlinx.coroutines.Deferred

@Dao
interface KeyHolderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(data: KeyHolder)

    @Query("SELECT * FROM KeyHolder LIMIT 1")
    fun get(): KeyHolder?

    @Delete
    fun delete(configData: KeyHolder)
}