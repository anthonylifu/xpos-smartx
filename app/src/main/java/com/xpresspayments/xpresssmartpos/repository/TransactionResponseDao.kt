package com.xpresspayments.xpresssmartpos.repository

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import androidx.room.*
import com.xpresspos.epmslib.entities.TransactionResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single


@Dao
interface TransactionResponseDao {
    @Insert
    fun save(vararg transactions: TransactionResponse)

    @Update
    fun update(response: TransactionResponse)

    @Delete
    fun delete(vararg transactions: TransactionResponse)

    @Delete
    fun delete(transactions: List<TransactionResponse>)


    @Query("SELECT * FROM TransactionResponse ORDER BY transactionTimeInMillis DESC")
    fun getAll(): DataSource.Factory<Int, TransactionResponse>

    @Query("SELECT * FROM TransactionResponse WHERE RRN LIKE :searchText OR STAN LIKE :searchText ORDER BY transactionTimeInMillis DESC")
    fun findTransaction(searchText: String):  DataSource.Factory<Int, TransactionResponse>


    @Query("SELECT * FROM TransactionResponse")
    fun getList(): List<TransactionResponse>


    @Query("SELECT * FROM TransactionResponse WHERE isNotified = :status")
    fun getByNotificationStatus(status: Boolean): List<TransactionResponse>


    @Query("SELECT * FROM TransactionResponse WHERE RRN LIKE :RRN")
    fun getWhere(RRN: String):  Flowable<List<TransactionResponse>>

    @Query("SELECT * FROM TransactionResponse WHERE RRN LIKE :RRN LIMIT 1")
    fun getOneSingle(RRN: String): Single<TransactionResponse>

    @Query("SELECT * FROM TransactionResponse WHERE RRN LIKE :RRN LIMIT 1")
    fun getOne(RRN: String): TransactionResponse?

}