package com.xpresspayment.morefun10Q

import android.os.Bundle
import android.os.RemoteException
import com.morefun.yapi.ServiceResult
import com.morefun.yapi.device.pinpad.DispTextMode
import com.morefun.yapi.device.pinpad.OnPinPadInputListener
import com.morefun.yapi.device.pinpad.PinAlgorithmMode
import com.xpresspos.epmslib.extensions.formatCurrencyAmount

object PinHandler {

    fun inputOnlinePin(pan: String, amount: Long, listener: OnInputPinListener) {
        logTrace("inputOnlinePin:$pan")

        val panBlock = pan.toByteArray()
       val bundle =  Bundle().apply {
            putString(
                "title_head_content",
                "Enter Pin (${(amount/100.0).formatCurrencyAmount()})"
            )
        }

        try {
            DeviceHelper.getPinpad().inputOnlinePin(
                bundle,
                panBlock,
                0,
                PinAlgorithmMode.ISO9564FMT1,
                object : OnPinPadInputListener.Stub() {
                    @Throws(RemoteException::class)
                    override fun onInputResult(ret: Int, pinBlock: ByteArray?, ksn: String?) {
                        listener.onInputPin(pinBlock)
                    }

                    @Throws(RemoteException::class)
                    override fun onSendKey(keyCode: Byte) {
                        if (keyCode == ServiceResult.PinPad_Input_Cancel.toByte()) {
                            listener.onInputPin(null)
                        }
                    }
                })
        } catch (e: RemoteException) {
            e.printStackTrace()
            logTrace("Pin entry error: ${e.localizedMessage}")
        }
    }

    fun inputOfflinePin(pan: String, amount: Long, listener: OnInputPinListener) {
        logTrace("inputPin:$pan")

        val bundle =  Bundle().apply {
            putString(
                "title_head_content",
                "Enter Pin (${(amount/100.0).formatCurrencyAmount()})"
            )
        }

        try {
            val minLength = 4
            val maxLength = 12
            DeviceHelper.getPinpad().setSupportPinLen(intArrayOf(minLength, maxLength))
            DeviceHelper.getPinpad().inputText(bundle, object : OnPinPadInputListener.Stub() {
                @Throws(RemoteException::class)
                override fun onInputResult(ret: Int, pinBlock: ByteArray?, ksn: String?) {
                    listener.onInputPin(pinBlock)
                }

                @Throws(RemoteException::class)
                override fun onSendKey(keyCode: Byte) {
                    if (keyCode == ServiceResult.PinPad_Input_Cancel.toByte()) {
                        listener.onInputPin(null)
                    }
                }
            }, DispTextMode.PASSWORD)
        } catch (e: RemoteException) {
            e.printStackTrace()
            logTrace("Pin entry error: ${e.localizedMessage}")
        }
    }
}


interface OnInputPinListener {
    fun onInputPin(pinBlock: ByteArray?)
}
