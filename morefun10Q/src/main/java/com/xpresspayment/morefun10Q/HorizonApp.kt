package com.xpresspayment.morefun10Q

import com.xpresspayments.commons.Device

class HorizonApp: MorefunApp() {
    override val device: Device
        get() =  Device.HorizonK11


    override fun validateModel() {
        logTrace("Terminal model: $terminalModel")
        if (!terminalModel.contains("K11", true)) throw Error("Incompatible device")
    }
}