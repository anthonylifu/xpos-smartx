package com.xpresspayment.morefun10Q

import android.os.Bundle
import android.os.RemoteException
import androidx.appcompat.app.AppCompatActivity
import com.morefun.yapi.ServiceResult
import com.morefun.yapi.device.reader.icc.ICCSearchResult
import com.morefun.yapi.device.reader.icc.IccCardType
import com.morefun.yapi.device.reader.icc.IccReaderSlot
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener
import com.morefun.yapi.device.reader.mag.MagCardInfoEntity
import com.morefun.yapi.device.reader.mag.OnSearchMagCardListener
import com.morefun.yapi.emv.EmvChannelType
import com.xpresspayment.morefun10Q.utils.EmvUtil
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.commons.Reader
import com.xpresspayments.commons.displayMessageId
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.HostConfig
import com.xpresspos.epmslib.entities.TransactionRequestData

class Morefun10CardProcessor(activity: AppCompatActivity, hostConfig: HostConfig) : EmvProcessor(
    activity,
    hostConfig
) {

    private val iccCardReader
        get() = DeviceHelper.getIccCardReader(IccReaderSlot.ICSlOT1)

    private val magCardReader
        get() = DeviceHelper.getMagCardReader()

    private  val rfReader
        get() = DeviceHelper.getIccCardReader(IccReaderSlot.RFSlOT)


    override fun startTransaction(requestData: TransactionRequestData, reader: Reader) {
        searchCard(reader, requestData)
    }

    override fun abortTransaction() {
        logTrace("AbortTransaction")
        stopSearch()
        setError(RuntimeException("Transaction cancelled"))
    }


    @Throws(RemoteException::class)
    fun searchCard(reader: Reader, requestData: TransactionRequestData) {
        setState(
            ProcessState(
                ProcessState.States.CARD_ENTRY,
                activity.getString(reader.displayMessageId())
            )
        )
        val listener: OnSearchIccCardListener.Stub = object : OnSearchIccCardListener.Stub() {
            @Throws(RemoteException::class)
            override fun onSearchResult(retCode: Int, bundle: Bundle) {
                stopSearch()
                if (retCode == ServiceResult.Success) {
                    val channel = if (bundle.getInt(ICCSearchResult.CARDOTHER) == IccReaderSlot.ICSlOT1) EmvChannelType.FROM_ICC else EmvChannelType.FROM_PICC
                    continueOffline(channel, requestData)
                } else {
                    setError(RuntimeException("Transaction cancelled"))
                }
            }
        }
        iccCardReader.searchCard(
            listener,
            60,
            arrayOf(IccCardType.CPUCARD, IccCardType.AT24CXX, IccCardType.AT88SC102)
        )
        rfReader.searchCard(
            listener,
            60,
            arrayOf(IccCardType.CPUCARD, IccCardType.AT24CXX, IccCardType.AT88SC102)
        )

        magCardReader.searchCard(object : OnSearchMagCardListener.Stub() {
            @Throws(RemoteException::class)
            override fun onSearchResult(retCode: Int, magCardInfoEntity: MagCardInfoEntity) {
                logTrace("magCardReader = $retCode")
                if (retCode == ServiceResult.Success) {
                   setState(
                        ProcessState(
                            ProcessState.States.PROCESSING,
                            "Processing"
                        )
                    )
                    MorefunMagstripeHandler(this@Morefun10CardProcessor, requestData).processOnline(magCardInfoEntity)
                } else {
                    setError(RuntimeException("Swipe error"))
                }
                stopSearch()
            }
        }, 60, Bundle())
    }


    private fun continueOffline(channel: Int, requestData: TransactionRequestData) {
        setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing"
            )
        )

        DeviceHelper.getEmvHandler().initTermConfig(EmvUtil.getInitTermConfig())
        DeviceHelper.getEmvHandler().emvProcess (
            EmvUtil.getInitBundleValue(channel,
                (requestData.amount/100.0).toString(),
                (requestData.otherAmount/100.00).toString()),
            MorefunEmvHandler(this, channel, requestData)
        )
    }

    fun stopSearch() {
        try {
            iccCardReader.stopSearch()
            rfReader.stopSearch()
            magCardReader.stopSearch()
        } catch (e: RemoteException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}