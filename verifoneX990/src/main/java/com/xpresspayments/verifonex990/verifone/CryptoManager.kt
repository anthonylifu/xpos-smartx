package com.xpresspayments.verifonex990.verifone

import com.xpresspos.epmslib.utils.TripleDES


object CryptoManager {

    fun getKCV(key: String) = TripleDES.encrypt("0000000000000000", key)
}