package com.xpresspayments.verifonex990.verifone

import android.os.Bundle
import com.xpresspayments.commons.Reader

val Reader.cardOption: Bundle
get() = Bundle().let {
    when (this) {
        Reader.CT, Reader.MANUAL -> it.putBoolean("supportSmartCard", true)
        Reader.CTLS ->it. putBoolean("supportCTLSCard", true)
        Reader.MSR ->it. putBoolean("supportMagCard", true)
        Reader.CT_MSR -> {
            it.putBoolean("supportSmartCard", true)
            it.putBoolean("supportMagCard", true)
        }
        Reader.CT_CTLS, Reader.CT_CTLS_MANUAL -> {
            it.putBoolean("supportCTLSCard", true)
            it.putBoolean("supportSmartCard", true)
        }
        Reader.CT_CTLS_MSR, Reader.CT_CTLS_MSR_MANUAL  ->{
            it.putBoolean("supportCTLSCard", true)
            it.putBoolean("supportSmartCard", true)
            it.putBoolean("supportMagCard", true)
        }
    }

    it
}
