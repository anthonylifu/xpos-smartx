package com.xpresspayments.verifonex990.verifone.extensions

import android.content.Context
import android.os.Bundle
import android.os.RemoteException
import android.util.Log
import com.vfi.smartpos.deviceservice.aidl.IBeeper
import com.vfi.smartpos.deviceservice.aidl.IEMV
import com.vfi.smartpos.deviceservice.aidl.PinInputListener
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad
import com.xpresspayments.verifonex990.R
import com.xpresspayments.verifonex990.verifone.emv.TAG
import com.xpresspayments.verifonex990.vfiService


fun Context.startPlatformPinEntry(
    isOnlinePin: Boolean,
    retryTimes: Int,
    cardPan: String,
    pinInputListener: PinInputListener,
    workingKeyId: Int,
    pinLimit: ByteArray = byteArrayOf(4, 5, 6, 8, 10, 12),
    keys: ByteArray = byteArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    timeout: Int = 20
) {
    val pinPad = vfiService.pinPad!!

    val param = Bundle()
    val globeParam = Bundle()
    param.putString("promptString", getString(R.string.enter_card_pin))
    param.putString("promptsFont", "/system/fonts/DroidSans-Bold.ttf")
    param.putByteArray("displayKeyValue", keys)
    param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit)
    param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, timeout)
    param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin)
    param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, cardPan)
    param.putInt(
        ConstIPinpad.startPinInput.param.KEY_desType_int,
        ConstIPinpad.startPinInput.param.Value_desType_3DES
    )
    if (!isOnlinePin) {
        param.putString(
            ConstIPinpad.startPinInput.param.KEY_promptString_String,
            "OFFLINE PIN, tries left: $retryTimes"
        )
    }
//        globeParam.putString(ConstIPinpad.startPinInput.globleParam.KEY_Display_One_String, "[1]")
    try {
        pinPad.startPinInput(workingKeyId, param, globeParam, pinInputListener)
    } catch (e: RemoteException) {
        e.printStackTrace()
    }
}


fun logTrace(message: String) = com.xpresspayments.commons.logTrace(message) //Log.d(TAG, message)

fun IEMV.clearAllAIDs() = updateAID(ConstIPBOC.updateAID.operation.clear, ConstIPBOC.updateAID.aidType.smart_card, null)
        && updateAID(ConstIPBOC.updateAID.operation.clear, ConstIPBOC.updateAID.aidType.contactless, null)

fun IEMV.clearAllRIDs() = updateRID(ConstIPBOC.updateRID.operation.clear, null)

fun IBeeper.errorBeep() {
    startBeep(600)
}

fun IBeeper.okBeep() {
    startBeep(200)
}


fun getTransactionResultString(result: Int) = when (result) {
    0, 204 -> "Accepted"
    1 -> "Declined"
    8 -> "EMV No Application"
    9 -> "EMV Complete"
    11 -> "Transaction aborted"
    12 -> "Fallback"
    13 -> "Data authentication failed"
    14 -> "Application blocked"
    15 -> "Not EMV card"
    16 -> "Unsupported EMV card"
    17 -> "Allowed amount exceeded"
    18 -> "Set parameter failed on 9F7A"
    19 -> "PAN does not match Track 2"
    20 -> "Cardholder validation error"
    21 -> "Transaction rejected"
    22 -> "Insufficient balance"
    23 -> "Amount exceeded Contactless limit"
    24 -> "Check card failed"
    25 -> "Card blocked"
    26 -> "Multiple card conflict"
    60 -> "Tap card failed"
    202 -> "Contactless transaction declined"
    203 -> "Contactless error"
    205 -> "Fallback - Insert Card"
    206 -> "No application found"
    207 -> "Not a chip card"
    208 -> "Transaction aborted"
    150 -> "Paypass completed. Please check the result on the phone"
    301 -> "Contactless kernel initialization failed"
    else -> "Error"
}


